#include <stdio.h>
#include <cstdlib>
#include "BMPFile.h"

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("Usage: mmp_dump file.mmp\n");
		return 1;
	}

	FILE *f = fopen(argv[1], "rb");
	if (f == NULL) {
		printf("Couldn't open %s\n", argv[1]);
		return 1;
	}

	int ntextures;
	if (fread(&ntextures, 4, 1, f) == 1)
		printf("File %s has %d textures\n", argv[1], ntextures);
	else {
		printf("Couldn't read number of textures.\n\
				Is the file %s empty ?\n", argv[1]);
		return 1;
	}

	BMPFile bmp;

	for (int i=0; i<ntextures; ++i)
	{
		int two;
		if (fread(&two, 2, 1, f) != 1) {
			printf("Error reading texture Nº%d of %d\n",
					i+1, ntextures);
			return 1;
		}

		int checksum;
		if (fread(&checksum, 4, 1, f) != 1) {
			printf("Error reading checksum of texture Nº%d of %d\n",
					i+1, ntextures);
			return 1;
		}

		unsigned int size;
		if (fread(&size, 4, 1, f) != 1) {
			printf("Error reading the size of texture Nº%d of %d\n",
					i+1, ntextures);
			return 1;
		}

		unsigned int len;
		if (fread(&len, 4, 1, f) != 1) {
			printf("Error reading the mame len of texture Nº%d of %d\n",
					i+1, ntextures);
			return 1;
		}
		char *name = (char *) malloc(len+5);
		if (fread(name, 1, len, f) != len) {
			printf("Error reading the name of texture Nº%d of %d\n",
					i+1, ntextures);
			free(name);
			return 1;
		}
		name[len] = '.';
		name[len+1] = 'b';
		name[len+2] = 'm';
		name[len+3] = 'p';
		name[len+4] = '\0';

		unsigned int type;
		if (fread(&type, 4, 1, f) != 1) {
			printf("Error reading the type of texture Nº%d of %d \
					named \"%s\".\n", i+1, ntextures, name);
			free(name);
			return 1;
		}

		int width, height;
		if (fread(&width, 4, 1, f) != 1) {
			printf("Error reading width of texture Nº%d of %d \
					named \"%s\".\n", i+1, ntextures, name);
			return 1;
		}
		if (fread(&height, 4, 1, f) != 1) {
			printf("Error reading height of texture Nº%d of %d \
					named \"%s\".\n", i+1, ntextures, name);
			return 1;
		}

		unsigned int data_len = size - 12;
		unsigned char *data = (unsigned char *) malloc(data_len);
		if (fread(data, 1, data_len, f) != data_len) {
			printf("Error reading bitmap of texture Nº%d of %d \
					named \"%s\".\n", i+1, ntextures, name);
			return 1;
		}


		printf("Saving %s, (%dx%d, type %d, size %d)", name, width, height, type, size);

		// Write the bitmap
		switch (type) {
		case 1:
			{
				printf("paletted)\n");
				unsigned char *palette = data + width*height;//data_len - 768+3;
				unsigned char *truecolour = (unsigned char *) malloc(width*height*3);
				for (int j=0; j<height; ++j)
					for (int k=j*width; k<(j+1)*width; ++k) {
						truecolour[3*k] = palette[data[k]*3] << 2;
						truecolour[3*k+1] = palette[data[k]*3+1] << 2;
						truecolour[3*k+2] = palette[data[k]*3+2] << 2;
					}
				bmp.SaveBMP(name, truecolour, width, height);
				free(truecolour);
				break;
			}
		case 2:
			{
				printf("alpha)\n");
				unsigned char *truecolour = (unsigned char *) malloc(width*height*3);
				for (int j=0; j<height; ++j)
					for (int k=j*width; k<(j+1)*width; ++k) {
						truecolour[3*k]   = data[k];
						truecolour[3*k+1] = data[k];
						truecolour[3*k+2] = data[k];
					}
				bmp.SaveBMP(name, truecolour, width, height);
				free(truecolour);
				break;
			}
		case 4:
			{
				printf("true colour)\n");
				bmp.SaveBMP(name, data, width, height);
				break;
			}
		default:
			printf("Sorry, I don't know the type %d of texture %s\n", type, name);
		}

		free(data);
		free(name);
	}



	return 0;
}
