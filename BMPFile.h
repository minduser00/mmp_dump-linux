#ifndef BMPFILE_H_
#define BMPFILE_H_

#include "mst_bmp.h"
#include <stdio.h>

class BMPFile {
public:
	int SaveBMP(const char *filename, unsigned char *buffer, int width, int height);

};

#endif
