/****************************************************************************
 * bmp.h
 *
 * BMP-related data types based on Microsoft's own.
 ***************************************************************************/

#ifndef __MST_BMP_H
#define __MST_BMP_H

#include <stdint.h>


/*
 * Tipos de datos
 *
 * En esta seccion definimos tipos de datos de tamaño fijo basado
 * en nombres Amiga C/C++
 *
 */

typedef uint8_t  UBYTE;
typedef uint32_t ULONG;
typedef int32_t  LONG;
typedef uint16_t UWORD;


/*
 * BITMAPFILEHEADER
 *
 * The BITMAPFILEHEADER structure contains information about the type, size,
 * and layout of a file that contains a DIB [device-independent bitmap].
 *
 */

typedef struct					/**** BMP file header structure ****/
{
	UWORD 	bfType;				/* Numero que identifica el tipo de archivo "MB" */
	ULONG	bfSize;				/* Tamaño del archivo */
	UWORD	bfReserved1;		/* Reservado */
	UWORD	bfReserved2;		/* ... */
	ULONG	bfOffBits;			/* Offset to bitmap data */
} __attribute__((__packed__))
BITMAPFILEHEADER;

# define BF_TYPE 0x4D42			/* "MB" */

/*
 * BITMAPINFOHEADER
 *
 * The BITMAPINFOHEADER structure contains information about the
 * dimensions and color format of a DIB [device-independent bitmap].
 *
 */

typedef struct					/**** BMP file info structure ****/
{
	ULONG	biSize;				/* Size of info header */
	LONG	biWidth;			/* Width of image */
	LONG	biHeight;			/* Height of image */
	UWORD	biPlanes;			/* Number of color planes */
	UWORD	biBitCount;			/* Number of bits per pixel */
	ULONG	biCompression;		/* Type of compression to use */
	ULONG	biSizeImage;		/* Size of image data */
	LONG	biXPelsPerMeter;	/* X pixels per meter */
	LONG	biYPelsPerMeter;	/* Y pixels per meter */
	ULONG	biClrUsed;			/* Number of colors used */
	ULONG	biClrImportant;		/* Number of important colors */
} __attribute__((__packed__))
BITMAPINFOHEADER;

/*
 * Constants for the biCompression field...
 */

#define	BI_RGB			0			/* No compression - straight BGR data */
#define BI_RLE8			1			/* 8-bit run-length compression */
#define BI_RLE4			2			/* 4-bit run-length compression */
#define BI_BITFIELDS	3			/* RGB bitmap with RGB masks */

/*
 * RGBTRIPLE
 *
 * This structure describes a color consisting of relative intensities of
 * red, green, and blue.
 *
 * Adapted from http://msdn.microsoft.com/en-us/library/aa922590.aspx.
 */

typedef struct
{
    UBYTE  rgbtBlue;
    UBYTE  rgbtGreen;
    UBYTE  rgbtRed;
} __attribute__((__packed__))
RGBTRIPLE;

typedef struct						/** Colormap entry structure **/
{
	UBYTE	rgbBlue;				/* Blue value */
	UBYTE	rgbGreen;				/* Green value */
	UBYTE	rgbRed;					/* Red value */
	UBYTE	rgbReserved;			/* Reserved */
}  __attribute__((__packed__))
RGBQUAD;

class Bitmap {
public:
    Bitmap(const char* filename);
    ~Bitmap();
    RGBQUAD* pixels;
    BITMAPFILEHEADER fh;
    BITMAPINFOHEADER ih;

    private:

};

#endif		/** __MST_BMP_H **/
