CC = gcc
CPPFLAGS = -g -Wall -pedantic -O2
CFLAGS = -g -Wall -pedantic -O2
PROG = mmp_dump
OBJ = mmp_dump.o BMPFile.o

$(PROG): $(OBJ)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJ)

BMPFile.o: BMPFile.cpp BMPFile.h mst_bmp.h
mmp_dump.o: mmp_dump.cpp BMPFile.h mst_bmp.h

install: $(PROG)
	@echo installing $(PROG)...
	@cp mmp_dump /usr/bin/mmp_dump
	@echo ...done

.PHONY: uninstall
uninstall:
	@echo uninstalling...
	@rm -rf /usr/bin/mmp_dump
	@echo ...done

.PHONY: clean
clean:
	@echo deleting files...
	@rm -rf mmp_dump *.o
	@echo ...done
