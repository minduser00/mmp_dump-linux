#include "BMPFile.h"
#include <cstdlib>
#include <assert.h>

int BMPFile::SaveBMP(const char *filename,
					 unsigned char *buffer,
					 int width,
					 int height)
{
	FILE *f = fopen(filename, "wb");
	assert(f != NULL);

	// BITMAP FILE HEADER

	BITMAPFILEHEADER bfh;
	bfh.bfType = ((UWORD) ('M' << 8) | 'B');
	bfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + width*height*3;
	bfh.bfReserved1 = 0;
	bfh.bfReserved2 = 0;
	bfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	fwrite(&bfh, sizeof(BITMAPFILEHEADER), 1, f);


	// BITMAPINFOHEADER

	BITMAPINFOHEADER bih;
	bih.biSize = sizeof(BITMAPINFOHEADER);
	bih.biWidth = width;
	bih.biHeight = height;
	bih.biPlanes = 1;
	bih.biBitCount = 24;
	bih.biCompression = BI_RGB;
	bih.biSizeImage = 0; // Set to zero for uncompressed
	bih.biXPelsPerMeter = 0;
	bih.biYPelsPerMeter = 0;
	bih.biClrUsed = 0;
	bih.biClrImportant = 0;

	fwrite(&bih, sizeof(BITMAPINFOHEADER), 1, f);

	uint8_t *buf = buffer + width * 3 * height;
	size_t lineBufLen=(width * sizeof(RGBTRIPLE));
	RGBTRIPLE *bgr, *line = (RGBTRIPLE *) malloc(lineBufLen);
	for (int i=height-1; i>=0; --i) {
		bgr = line + width;
		for (int j=0; j<width; ++j) {
			(--bgr)->rgbtBlue = *--buf;
			bgr->rgbtGreen = *--buf;
			bgr->rgbtRed = *--buf;
		}
		fwrite(line, lineBufLen, 1, f);
	}
	free(line);
	fclose(f);

	return 0;
}
