Comando mmp_dump para extraer archivos .mmp, todas las texturas
estarán en archivos.bmp

				Compilacion
Para compilar la herramienta mmp_dump desde la consola escribe

	make

se creara el programa mmp_dump para usarlo puedes usar

	./mmp_dump archivo.mmp

siendo "archivo" el nombre de tu archivo a extraer
el resultado sera una o varias texturas en formato .bmp


				Instalacion
Para instalar el comando y poder usarlo desde cualquier lugar donde esten
tur archivos de texturas .mmp escribe en la shell:

	make
	sudo make install
	make clean

NOTA: make clean limpia archivos .o temporales y el programa mmp_dump
	  de la carpeta de compilacion

despues de intalarlo bastara escribir un:

	mmp_dump archivo.mmp


				Desinstalacion

Para desinstalar el programa mmp_dump:

	sudo make uninstall


